public class Taxi {

    int id;
    String ownerName;
    String carBrand;
    private boolean isFree = true;
    private User user;
    private int[] coordinates;

    public Taxi(String ownerName, String carBrand){
        isFree = true;
        this.ownerName = ownerName;
        this.carBrand = carBrand;
    }

    public void setFreeOrNot(boolean isFree){
        this.isFree = isFree;
    }
    public boolean isFree(){
        return this.isFree;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public int[] getCoordinates() {
        return coordinates;
    }
    public void setCoordinates(int[] coordinates) {
        this.coordinates = coordinates;
    }



}
