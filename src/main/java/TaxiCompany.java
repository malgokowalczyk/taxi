import java.util.LinkedList;

class TaxiCompany {

    private LinkedList<Taxi> taxis = new LinkedList<Taxi>();

    void addTaxi(String ownerName, String carBrand){
        Taxi taxi = new Taxi(ownerName,carBrand);
        taxi.id = taxis.size();
        taxis.add(taxi);
    }

    String assignTaxi(User user){

        for (Taxi taxi : taxis){
            if (taxi.isFree()) {
                taxi.setFreeOrNot(false);
                taxi.setUser(user);
                taxi.setCoordinates(user.getCoordinates());
                return ("You have been assigned a taxi nr " + taxi.id + " owned by " + taxi.ownerName + ", the car is " + taxi.carBrand);
            }
        }
        return ("There are no free taxis :(");

    }





}
