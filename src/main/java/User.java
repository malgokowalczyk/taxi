public class User {

    private String name;
    private int[]coordinates;

    public User(String name, int[] coordinates){
        this.name = name;
        this.coordinates = coordinates;
    }

    public void callTaxi(TaxiCompany comp){
        System.out.println(comp.assignTaxi(this));
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int[] getCoordinates() {
        return coordinates;
    }
    public void setCoordinates(int[] coordinates) {
        this.coordinates = coordinates;
    }
}
