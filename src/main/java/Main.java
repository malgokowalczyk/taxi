public class Main {

    public static void main(String[]args) {

        TaxiCompany comp = new TaxiCompany();
        comp.addTaxi("Tomek", "Panda");
        comp.addTaxi("Wojtek", "Honda");
        User malgosia = new User("Malgosia", new int[]{12, 24});
        malgosia.callTaxi(comp);
        malgosia.callTaxi(comp);
        malgosia.callTaxi(comp);
    }

}
